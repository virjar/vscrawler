package com.virjar.vscrawler.core.grab;

import com.virjar.vscrawler.core.grab.model.GrabRequest;
import com.virjar.vscrawler.core.grab.model.GrabResult;
import com.virjar.vscrawler.core.net.session.CrawlerSession;

public interface GrabProcessor {
    GrabResult grab(GrabRequest grabRequest, CrawlerSession crawlerSession);
}
