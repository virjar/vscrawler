package com.virjar.vscrawler.core.grab.databind;


import com.virjar.vscrawler.core.grab.model.GrabRequest;
import com.virjar.vscrawler.core.grab.multiaction.ActionRequestHandler;

/**
 * Created by virjar on 2019/1/25.<br>
 * do not need bind attribute，use singleton instance
 */
public class DirectMapGenerator implements ActionRequestHandlerGenerator {
    private ActionRequestHandler delegate;

    public DirectMapGenerator(ActionRequestHandler delegate) {
        this.delegate = delegate;
    }

    @Override
    public ActionRequestHandler gen(GrabRequest invokeRequest) {
        return delegate;
    }
}