package com.virjar.vscrawler.core.grab.databind;

import com.virjar.vscrawler.core.grab.model.GrabRequest;
import com.virjar.vscrawler.core.grab.multiaction.ActionRequestHandler;

/**
 * Created by virjar on 2019/1/25.<br>
 * emptyActionRequestHandlerCreateHelper
 */

public class EmptyARCreateHelper implements ActionRequestHandlerGenerator {
    private Class<? extends ActionRequestHandler> actionRequestHandlerClass;

    public EmptyARCreateHelper(Class<? extends ActionRequestHandler> actionRequestHandlerClass) {
        this.actionRequestHandlerClass = actionRequestHandlerClass;
    }

    @Override
    public ActionRequestHandler gen(GrabRequest invokeRequest) {
        try {
            return actionRequestHandlerClass.newInstance();
        } catch (Exception e) {
            // not happen
            throw new IllegalStateException(e);
        }
    }
}
