package com.virjar.vscrawler.core.grab.multiaction;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface GrabAction {
    String value();
}
