package com.virjar.vscrawler.core.grab.databind;
import lombok.Getter;

/**
 * Created by virjar on 2019/1/25.<br>
 * exception when transfer a value to bind field
 */
public class DataParseFailedException extends RuntimeException {
    @Getter
    private String attributeName;

    @Getter
    private Exception exception;


    public DataParseFailedException(String attributeName, Exception exception) {
        this.attributeName = attributeName;
        this.exception = exception;
    }
}
