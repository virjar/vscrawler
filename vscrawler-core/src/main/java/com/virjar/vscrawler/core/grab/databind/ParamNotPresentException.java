package com.virjar.vscrawler.core.grab.databind;

import lombok.Getter;

/**
 * Created by virjar on 2019/1/25.<br>
 * assert if some param not presented
 */

public class ParamNotPresentException extends RuntimeException {
    @Getter
    private String attributeName;

    public ParamNotPresentException(String attributeName) {
        this.attributeName = attributeName;
    }
}
