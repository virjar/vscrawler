package com.virjar.vscrawler.core.grab.model;

import com.virjar.vscrawler.core.util.CommonUtils;
import lombok.Data;

@Data
public class GrabResult {

    public static final int statusOK = 0;
    public static final int statusFailed = -1;
    public static final int statusException = -2;
    public static final int statusReturnNull = -3;

    private int status;

    private Object data;
    private String errorMessage;

    public GrabResult(int status, Object data, String errorMessage) {
        this.status = status;
        this.data = data;
        this.errorMessage = errorMessage;
    }

    public static GrabResult success(Object data) {
        return new GrabResult(statusOK, data, null);
    }

    public static GrabResult failed(int status, String errorMessage) {
        return new GrabResult(status, null, errorMessage);
    }


    public static GrabResult failed(Throwable throwable) {
        return new GrabResult(statusException, null, CommonUtils.getStackTrack(throwable));
    }

    public static GrabResult failed(String errorMessage) {
        return new GrabResult(statusFailed, null, errorMessage);
    }
}
