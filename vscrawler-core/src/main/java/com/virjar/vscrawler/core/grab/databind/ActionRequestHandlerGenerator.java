package com.virjar.vscrawler.core.grab.databind;

import com.virjar.vscrawler.core.grab.model.GrabRequest;
import com.virjar.vscrawler.core.grab.multiaction.ActionRequestHandler;

/**
 * Created by virjar on 2019/1/25.<br>
 * create a handler  to deal with a certain request,create and auto bind value
 */
public interface ActionRequestHandlerGenerator {
    ActionRequestHandler gen(GrabRequest invokeRequest);
}
