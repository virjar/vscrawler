package com.virjar.vscrawler.core.grab.databind;

import com.virjar.vscrawler.core.grab.model.GrabRequest;
import com.virjar.vscrawler.core.grab.multiaction.ActionRequestHandler;

import java.lang.reflect.Constructor;

/**
 * Created by virjar on 2019/1/25.<br>
 * InvokeRequestConstructorActionRequestHandlerCreateHelper
 */

public class ICRCreateHelper implements ActionRequestHandlerGenerator {
    private Constructor<? extends ActionRequestHandler> theConstructor;

    public ICRCreateHelper(Constructor<? extends ActionRequestHandler> theConstructor) {
        this.theConstructor = theConstructor;
    }

    @Override
    public ActionRequestHandler gen(GrabRequest invokeRequest) {
        try {
            return theConstructor.newInstance(invokeRequest);
        } catch (Exception e) {
            // not happen
            throw new IllegalStateException(e);
        }
    }
}
