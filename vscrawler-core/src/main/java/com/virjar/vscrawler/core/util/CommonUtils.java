package com.virjar.vscrawler.core.util;

import com.google.common.base.Charsets;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class CommonUtils {

    public static String safeToString(Object input) {
        if (input == null) {
            return null;
        }
        return input.toString();
    }


    public static String translateSimpleExceptionMessage(Throwable exception) {
        String message = exception.getClass().getName();
        if (exception.getMessage() != null && exception.getMessage().trim().length() >= 0) {
            message += ":" + exception.getMessage().trim();
        }
        return message;
    }

    public static String getStackTrack(Throwable throwable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(byteArrayOutputStream));
        throwable.printStackTrace(printWriter);
        printWriter.close();
        // return byteArrayOutputStream.toString(Charsets.UTF_8);
        return new String(byteArrayOutputStream.toByteArray(), Charsets.UTF_8);
    }
}
