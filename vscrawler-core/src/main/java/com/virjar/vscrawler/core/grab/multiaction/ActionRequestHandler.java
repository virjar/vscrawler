package com.virjar.vscrawler.core.grab.multiaction;


import com.virjar.vscrawler.core.grab.model.GrabRequest;

/**
 * handle处理抽象，针对于不同action的路由处理
 */
public interface ActionRequestHandler {
    Object handleRequest(GrabRequest invokeRequest);
}
